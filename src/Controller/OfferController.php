<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Transaction;
use App\Entity\User;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use Doctrine\ORM\Mapping\Id;
use Proxies\__CG__\App\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class OfferController extends AbstractController
{
    #[Route('/offer/index/{id}', name: 'offer_profile', methods: ['GET'])]
    public function profile(OfferRepository $offerRepository, User $user): Response
    {
        return $this->render('offer/UserOffer.html.twig', [
            'user' => $user,
            'offers' => $offerRepository->findAll(),
        ]);
    }

    #[Route('/offer/all', name: 'offer_index', methods: ['GET'])]
    public function index(OfferRepository $offerRepository): Response
    {
        return $this->render('offer/index.html.twig', [
            'offers' => $offerRepository->findAll(),
        ]);
    }

    #[Route('/profile', name: 'offer_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {   
        $user=$this->getUser();
        $kwPrice = 5;
        $offer = new Offer();
        $offer->setUser($user);
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $offer->setCredits($offer->getQuantityKW() * $kwPrice);
            $user->setQuantityKw($user->getQuantityKw() - $offer->getQuantityKW());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($offer);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('offer_show',["id"=>$offer->getId()]);
        }

        return $this->render('offer/new.html.twig', [
            'user'=>$user,
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/offer/{id}', name: 'offer_show', methods: ['GET'])]
    public function show(Offer $offer): Response
    {
        return $this->render('offer/show.html.twig', [
            'user'=>$this->getUser(),
            'offer' => $offer,
        ]);
    }

    #[Route('/offer/{id}/edit', name: 'offer_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Offer $offer): Response
    {
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('offer_index');
        }

        return $this->render('offer/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/offer/{id}', name: 'offer_delete', methods: ['POST'])]
    public function delete(Request $request, Offer $offer): Response
    {
        $user= $this->getUser();
        if ($this->isCsrfTokenValid('delete'.$offer->getId(), $request->request->get('_token'))) {
            $user->setQuantityKw($user->getQuantityKw() + $offer->getQuantityKW());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($offer);
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('offer_index');
    }
}
