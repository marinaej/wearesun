<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OfferRepository;

class IndexController extends AbstractController
{
    #[Route('/home', name: 'index')]
    public function index(OfferRepository $offerRepository): Response
    {
        $offers = $offerRepository->findTwoOffers();
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'offers' => $offers
        ]);
    }
    #[Route('/contact', name: 'contact')]
    public function contact(): Response
    {
        return $this->render('contact.html.twig', [
        ]);
    }
}
