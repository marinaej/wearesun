<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Transaction;
use App\Entity\User;
use App\Form\TransactionType;
use App\Repository\OfferRepository;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/transaction')]
class TransactionController extends AbstractController
{
    #[Route('/index/{id}', name: 'transaction_user', methods: ['GET'])]
    public function userTransaction(TransactionRepository $transactionRepository,User $user): Response 
    {
        return $this->render('transaction/userTransaction.html.twig', [
            'user'=> $user,
            'transactions' => $transactionRepository->findAll(),
        ]);
    }
    #[Route('/index', name: 'transaction_index', methods: ['GET'])]
    public function index(TransactionRepository $transactionRepository): Response 
    {
        return $this->render('transaction/index.html.twig', [
            /**TODO findbyUserID */
            'transactions' => $transactionRepository->findAll(),
        ]);
    }

    #[Route('/new/{id}', name: 'transaction_new', methods: ['GET', 'POST'])]
    public function new(Offer $offer): Response
    {
        $user = $this->getUser();
        $userCredited = $offer->getUser();
        $credits = $offer->getCredits();
        $quantityKW = $offer->getQuantityKW();
        $transaction = new Transaction();
        $transaction
            ->setQuantityOfKW($quantityKW)
            ->setPrice($credits)
            ->setTransactionFrom($user)
            ->setTransactionTo($userCredited);
        //set credits
        $user->setCredits($user->getCredits() - $credits);
        $user->setQuantityKw($user->getQuantityKw() + $quantityKW);
        $userCredited->setCredits($userCredited->getCredits() + $credits);
        $offer->setStatus(true);
        // persists
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->persist($userCredited);
        $entityManager->persist($offer);
        $entityManager->persist($transaction);
        $entityManager->flush();

        return $this->redirectToRoute('transaction_validation',['id' => $offer->getId()]);
    }
    #[Route('/validation/{id}', name: 'transaction_validation', methods: ['GET'])]
    public function validation(Offer $offer): Response
    {
        return $this->render('transaction/validation.html.twig', [
            'user'=> $this->getUser(),
            'offer' => $offer,
        ]);
    }
    #[Route('/{id}', name: 'transaction_show', methods: ['GET'])]
    public function show(Transaction $transaction, Offer $offer): Response
    {
        return $this->render('transaction/show.html.twig', [
            'offer' => $offer,
            'transaction' => $transaction,
        ]);
    }

    #[Route('/{id}/edit', name: 'transaction_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Transaction $transaction): Response
    {
        $form = $this->createForm(Transaction1Type::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()
                ->getManager()
                ->flush();

            return $this->redirectToRoute('transaction_index');
        }

        return $this->render('transaction/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'transaction_delete', methods: ['POST'])]
    public function delete(Request $request, Transaction $transaction): Response
    {
        if (
            $this->isCsrfTokenValid(
                'delete' . $transaction->getId(),
                $request->request->get('_token')
            )
        ) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($transaction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transaction_index');
    }
}
