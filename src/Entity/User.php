<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="integer")
     */
    private $ClientNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $credits;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="user")
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class, inversedBy="users")
     */
    private $provider;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="transactionFrom")
     */
    private $transactionReceived;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="transactionTo")
     */
    private $transactionSent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantityKw;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="integer")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;



    public function __construct()
    {
        $this->offer = new ArrayCollection();
        $this->transactionReceived = new ArrayCollection();
        $this->transactionSent = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->email;
        return $this->provider;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    public function getClientNumber(): ?int
    {
        return $this->ClientNumber;
    }

    public function setClientNumber(int $ClientNumber): self
    {
        $this->ClientNumber = $ClientNumber;

        return $this;
    }

    public function getCredits(): ?int
    {
        return $this->credits;
    }

    public function setCredits(?int $credits): self
    {
        $this->credits = $credits;

        return $this;
    }



    /**
     * @return Collection|offer[]
     */
    public function getOffer(): Collection
    {
        return $this->offer;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offer->contains($offer)) {
            $this->offer[] = $offer;
            $offer->setUser($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offer->removeElement($offer)) {
            // set the owning side to null (unless already changed)
            if ($offer->getUser() === $this) {
                $offer->setUser(null);
            }
        }

        return $this;
    }

    public function getProvider(): ?provider
    {
        return $this->provider;
    }

    public function setProvider(?provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return Collection|transaction[]
     */
    public function getTransactionReceived(): Collection
    {
        return $this->transactionReceived;
    }

    public function addTransactionReceived(transaction $transactionReceived): self
    {
        if (!$this->transactionReceived->contains($transactionReceived)) {
            $this->transactionReceived[] = $transactionReceived;
            $transactionReceived->setTransactionFrom($this);
        }

        return $this;
    }

    public function removeTransactionReceived(transaction $transactionReceived): self
    {
        if ($this->transactionReceived->removeElement($transactionReceived)) {
            // set the owning side to null (unless already changed)
            if ($transactionReceived->getTransactionFrom() === $this) {
                $transactionReceived->setTransactionFrom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactionSent(): Collection
    {
        return $this->transactionSent;
    }

    public function addTransactionSent(Transaction $transactionSent): self
    {
        if (!$this->transactionSent->contains($transactionSent)) {
            $this->transactionSent[] = $transactionSent;
            $transactionSent->setTransactionTo($this);
        }

        return $this;
    }

    public function removeTransactionSent(Transaction $transactionSent): self
    {
        if ($this->transactionSent->removeElement($transactionSent)) {
            // set the owning side to null (unless already changed)
            if ($transactionSent->getTransactionTo() === $this) {
                $transactionSent->setTransactionTo(null);
            }
        }

        return $this;
    }

    public function getQuantityKw(): ?int
    {
        return $this->quantityKw;
    }

    public function setQuantityKw(?int $quantityKw): self
    {
        $this->quantityKw = $quantityKw;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhoneNumber(): ?int
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(int $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }






    





}
