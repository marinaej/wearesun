<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantityOfKW;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="transactionReceived")
     */
    private $transactionFrom;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="transactionSent")
     */
    private $transactionTo;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantityOfKW(): ?int
    {
        return $this->quantityOfKW;
    }

    public function setQuantityOfKW(int $quantityOfKW): self
    {
        $this->quantityOfKW = $quantityOfKW;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTransactionFrom(): ?User
    {
        return $this->transactionFrom;
    }

    public function setTransactionFrom(?User $transactionFrom): self
    {
        $this->transactionFrom = $transactionFrom;

        return $this;
    }

    public function getTransactionTo(): ?User
    {
        return $this->transactionTo;
    }

    public function setTransactionTo(?User $transactionTo): self
    {
        $this->transactionTo = $transactionTo;

        return $this;
    }

}
