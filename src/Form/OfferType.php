<?php

namespace App\Form;

use App\Entity\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user=$options["data"]->getUser();
        $builder
            ->add('localisation')
            ->add('description')
            ->add('quantityKW', null, ['constraints'=> new Assert\Range([
                'min' => 1,
                'max' => is_null($user) ? 0 : $user->getQuantityKw(),
                'notInRangeMessage' => 'Energie en stock insufisante',
            ])])
                                
           
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
